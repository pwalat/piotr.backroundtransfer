﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Piotr.BackroundTransfer.App
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private DownloadOperation _activeDownload; 

        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += MainPageLoaded;
        }

        async void MainPageLoaded(object sender, RoutedEventArgs e)
        {
            await LoadActiveDownloadsAsync();
        }

        private async Task LoadActiveDownloadsAsync()
        {
            IReadOnlyList<DownloadOperation> downloads = null;
            downloads = await BackgroundDownloader.GetCurrentDownloadsAsync();
            if(downloads.Count > 0)
            {
                //for simplicity we support only one download
                await ResumeDownloadAsync(downloads.First());
            }
        }

        private async Task ResumeDownloadAsync(DownloadOperation downloadOperation)
        {
            DownloadButton.IsEnabled = false;
            _activeDownload = downloadOperation;
            var progress = new Progress<DownloadOperation>(ProgressCallback);
            await downloadOperation.AttachAsync().AsTask(progress);
        }

        private async void DownloadClick(object sender, RoutedEventArgs e)
        {
            const string fileLocation
             = "http://download.thinkbroadband.com/100MB.zip";
            var uri = new Uri(fileLocation);
            var downloader = new BackgroundDownloader();
            StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("100MB.zip",
                CreationCollisionOption.ReplaceExisting);
            DownloadOperation download = downloader.CreateDownload(uri, file);
            await StartDownloadAsync(download);
        }

        private void ProgressCallback(DownloadOperation obj)
        {
            double progress 
                = ((double) obj.Progress.BytesReceived / obj.Progress.TotalBytesToReceive);
            DownloadProgress.Value = progress * 100;
            if(progress >= 1.0)
            {
                _activeDownload = null;
                DownloadButton.IsEnabled = true;
            }
        }

        private async Task StartDownloadAsync(DownloadOperation downloadOperation)
        {
            DownloadButton.IsEnabled = false;
            _activeDownload = downloadOperation;
            var progress = new Progress<DownloadOperation>(ProgressCallback);
            await downloadOperation.StartAsync().AsTask(progress);
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
    }
}
